package ru.thisistails.quicky.DiscordExceptions;

import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import ru.thisistails.quicky.Tools.DiscordException;

public class InternalError extends DiscordException {

    public InternalError(boolean ephermal, SlashCommandInteractionEvent event) {
        super("Внутренняя ошибка", "Произошла непридвиденная ошибка при исполнении команды.", ephermal, event);
    }
    
}
