package ru.thisistails.quicky;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import ru.thisistails.quicky.Tools.DiscordCommand;

public class DiscordManager {
    
    private static DiscordManager instance;
    private static final Logger logger = LoggerFactory.getLogger(DiscordManager.class);

    private List<DiscordCommand> commands;
    private List<ListenerAdapter> adapters;

    public static DiscordManager getInstance() {
        if (instance == null) {
            instance = new DiscordManager();
        }
        return instance;
    }

    private DiscordManager() {
        commands = new ArrayList<>();
        adapters = new ArrayList<>();
    }

    /**
     * Регистрирует команду для дальнейшего использования.
     * @param command   Сама команда.
     * @apiNote         Регистрировать слушатель не нужно. DiscordCommand наследуется от ListenerAdapter
     *  и для работы требует регистрации как слушателя так, что команда уже работает как слушатель.
     */
    public void registerCommand(DiscordCommand command) {
        commands.add(command);
    }

    /**
     * Регистрирует слушатель для дальнейшего использования.
     * @param adapter   Сам слушатель.
     */
    public void registerListener(ListenerAdapter adapter) {
        adapters.add(adapter);
    }

    protected void initialize(JDA jda) {
        logger.info("Инициализация DiscordManager...");
        logger.info("Регистрация команд...");
        commands.stream().forEach((command) -> {
            jda.addEventListener(command);
            jda.upsertCommand(command.getSlashCommandData()).queue();
            logger.info("Команда " + command.getClass().getName() + " зарегистрирована.");
        });
        logger.info("Регистрация команд окончена.");
        logger.info("Регистрация слушателей...");
        adapters.stream().forEach((adapter) -> {
            jda.addEventListener(adapter);
            logger.info("Слушатель " + adapter.getClass().getName() + " зарегистрирован.");
        });
        logger.info("Регистрация слушателей окончена.");
        logger.info("Инициализация окончена.");
    }

}
