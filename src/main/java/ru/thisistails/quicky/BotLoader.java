package ru.thisistails.quicky;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.EnumSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import lombok.Setter;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Icon;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.MemberCachePolicy;

public class BotLoader {
    
    private static BotLoader instance;
    private @Getter @Setter String token;
    private @Getter JDA jda;
    private static final Logger logger = LoggerFactory.getLogger(BotLoader.class);
    /*
     * Сбрасывает все команды бота при запуске.
     */
    private @Getter @Setter boolean dropAllCommandsBeforeInit = false;

    private static final @Getter EnumSet<GatewayIntent> allIntents = EnumSet.allOf(GatewayIntent.class);

    private BotLoader() {
        token = null;
        jda = null;
    }

    public static BotLoader getInstance() {
        if (instance == null) 
            instance = new BotLoader();
        
        return instance;
    }

    /**
     * Устанавливает аватарку
     * @param avatar    Аватарка
     * @throws IOException Если не удалось прочитать файл
     * @apiNote GIF аватарки тоже можно ставить т.к боты имеют нитро.
     */
    public void setAvatar(File avatar) throws IOException {
        jda.getSelfUser().getManager().setAvatar(Icon.from(avatar)).queue(); 
    }

    /**
     * @param intents 
     * @apiNote ДОЛЖЕН выполнятся после {@link DiscordManager#registerCommand(ru.thisistails.quicky.Tools.DiscordCommand)}
    */
    public void login(Collection<GatewayIntent> intents, MemberCachePolicy policy) {
        logger.info("Запуск бота...");

        try {
            jda = JDABuilder.createDefault(token, intents)
            .setMemberCachePolicy(policy)
            .setAutoReconnect(true)
            .build()
            .awaitReady();
        } catch (InterruptedException e) {
            logger.error("Фатальная ошибка при запуске бота.", e);
            Runtime.getRuntime().exit(1);
        }
        logger.info("Бот запущен. Инициализация DiscordManager.");
        if (dropAllCommandsBeforeInit) {
            jda.updateCommands().queue();
        }
        DiscordManager.getInstance().initialize(jda);
    }

}
