package ru.thisistails.quicky.Tools;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.awt.Color;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Setter;

public abstract class DiscordException {

    private static final Logger logger = LoggerFactory.getLogger(DiscordException.class);
    protected final String title;
    protected @Setter boolean ephermal;
    private SlashCommandInteractionEvent event;
    private final String defaultMessage;

    /**
     * Создаёт класс для отправки ошибок.
     * @param title             Заголовок для embed.
     * @param defaultMessage    Сообщение по умолчанию для embed.
     * @param ephermal          Сообщение видно для всех или виден только для отправителя?
     * @param event             Сам ивент.
     */
    public DiscordException(String title, String defaultMessage, boolean ephermal, SlashCommandInteractionEvent event) {
        this.title = title;
        this.defaultMessage = defaultMessage;
        this.ephermal = ephermal;
        this.event = event;
    }

    protected MessageEmbed getEmbed(String message) {
        EmbedBuilder builder = new EmbedBuilder()
        .setTitle(title)
        .setDescription(message)
        .setColor(Color.red)
        .setTimestamp(new Date().toInstant());

        return builder.build();
    }

    public void send() {
        logger.warn("DiscordException: Вызвана ошибка \"" + title + "\".");
        event.replyEmbeds(getEmbed(defaultMessage)).setEphemeral(ephermal).queue();
    }

    public void send(String message) {
        logger.warn("DiscordException: Вызвана ошибка \"" + title + "\".");
        event.replyEmbeds(getEmbed(message)).setEphemeral(ephermal).queue();
    }

    public void send(String message, Throwable throwable) {
        logger.warn("DiscordException: Вызвана ошибка \"" + title + "\".", throwable);
        event.replyEmbeds(getEmbed(message)).setEphemeral(ephermal).queue();
    }

}
