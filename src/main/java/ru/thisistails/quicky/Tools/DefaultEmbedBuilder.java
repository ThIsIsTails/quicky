package ru.thisistails.quicky.Tools;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import net.dv8tion.jda.api.EmbedBuilder;

/**
 * Позволяет быстро сделать шаблон
 */
public class DefaultEmbedBuilder {
    
    private @Getter @Setter int color;
    private @Getter @Setter boolean alwaysMakeTimestamps = false;

    public EmbedBuilder getBuilder() {
        EmbedBuilder builder = new EmbedBuilder()
            .setColor(color);
        
        if (alwaysMakeTimestamps)
            builder.setTimestamp(new Date().toInstant());
        
        return builder;
    }

}
