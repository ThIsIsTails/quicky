package ru.thisistails.quicky.Tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

public class YamlManager {
    private static Logger logger = LoggerFactory.getLogger(YamlManager.class);
	
	public static Map<String, Object> getYaml(File requested) {
		Map<String, Object> req = null;
		try {
			Yaml yml = new Yaml();
			req = yml.load(new FileReader(requested));
		} catch(FileNotFoundException error) {
			logger.error("Файла " + requested.getName() + " не существует!");
			logger.error("Пытаемся создать новый.");

			try {
                logger.trace("Попытка копирования...");

                InputStream stream = YamlManager.class.getResourceAsStream("/" + requested.getName());
                
                FileUtils.copyInputStreamToFile(stream, requested);
                logger.info("Файл успешно создан.");
				return getYaml(requested);
                
            } catch (IOException e) {
                logger.error("Провал копирования файла " + requested.getName(), e);
            }
		}

		return req;
	}

}
