package ru.thisistails.quicky.Tools;

import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;

public abstract class DiscordCommand extends ListenerAdapter {

    public abstract SlashCommandData getSlashCommandData();
    public abstract void onSlashCommand(SlashCommandInteractionEvent slash);
    @Override
    public void onSlashCommandInteraction(SlashCommandInteractionEvent event) {
        if (!event.getName().equals(getSlashCommandData().getName())) return;

        onSlashCommand(event);
    }

}
